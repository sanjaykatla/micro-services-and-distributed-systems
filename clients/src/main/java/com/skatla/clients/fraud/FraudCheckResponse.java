package com.skatla.clients.fraud;

public record FraudCheckResponse(boolean fraudulentCustomer) {
}
