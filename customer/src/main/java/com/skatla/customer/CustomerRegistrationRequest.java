package com.skatla.customer;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email
) {
}
